from django.http import HttpResponseRedirect
from doctopyt.models import Creneau, RendezVous
from doctopyt.forms import CreateCreneauForm
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils import timezone
from django.http import HttpResponseRedirect, Http404
from .models import Specialite, Praticien, ZoneGeographique, Billet, Praticien_specialite
from .forms import PraticienForm, SearchPraticienForm, RegisterForm, BilletForm

def is_practicien(func):
    def check_user_is_practicien(request, *args, **kwargs):
        if request.user.is_authenticated and hasattr(request.user, 'praticien'):
            return func(request, *args, **kwargs)
        return redirect('doctopyt:index')
    return check_user_is_practicien

def index(request):
    return render(request, 'doctopyt/index.html')

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, ('Connexion réussie'))
            return HttpResponseRedirect(request.GET.getlist('next')[0]) if request.GET.getlist('next') else redirect('doctopyt:index')
        else:
            messages.success(request, ('Erreur de connexion, veuillez réessayer'))
            return redirect('doctopyt:login')
    else:
        return render(request, 'authentication/login.html', {})

def logout_user(request):
    logout(request)
    messages.success(request, ('Vous êtes déconnecté'))
    return redirect('doctopyt:index')

def register(request):
    form = RegisterForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(request, username=username, password=password)

            type = form.cleaned_data.get('type')
            if type == 'praticien':
                praticien = Praticien(user_id=user.id,name=username)
                praticien.save()

            login(request, user)
            messages.success(request, ('Vous êtes inscrit et connectés'))
            return redirect('doctopyt:me') if type == 'praticien' else redirect('doctopyt:index')
        else:
            form = RegisterForm()
            messages.success(request, ('Erreur dans l\'inscription, veuillez réessayer'))
            return redirect('doctopyt:register')
    else:
        return render(request, 'authentication/register.html', {'form':form,})

# Create your views here.
@login_required(login_url='/login_user/')
def me_specialites(request):
    user = request.user
    try:
      praticien = user.praticien
    except:
      return redirect('doctopyt:index')
    return render(request, 'doctopyt/me_specialites.html', {"praticien_specialites": praticien.praticien_specialite.all()})

@require_http_methods(["GET","POST"])
@login_required(login_url='/login_user/')
def me_billets(request, specialite_pk):
  user = request.user
  try:
    praticien = user.praticien
  except:
    return redirect('doctopyt:index')
  try:
    praticien_specialite = praticien.praticien_specialite.get(specialite_id=specialite_pk)
  except:
    raise(Http404)
  if request.method == 'GET':
      form = BilletForm()
      return render(request, 'doctopyt/billets.html', {
        "specialite": praticien_specialite.specialite,
        "billets": praticien_specialite.billets.all(),
        "praticien": praticien,
        "form": form,
        "me": True
      })
  else:
      form = BilletForm(request.POST)
      if form.is_valid():
          billet = form.save(commit=False)
          billet.praticien_specialite = praticien_specialite
          billet.save()
      return redirect('doctopyt:me_billets', specialite_pk=specialite_pk)

@require_http_methods(["GET","POST","DELETE"])
@login_required(login_url='/login_user/')
def me_billet(request,specialite_pk,billet_pk):
    user = request.user
    try:
        praticien = user.praticien
    except:
        return redirect('doctopyt:index')
    try:
        praticien_specialite = praticien.praticien_specialite.get(specialite_id=specialite_pk)
    except:
        raise(Http404)
    try:
        billet = praticien_specialite.billets.get(id=billet_pk)
    except:
        raise(Http404)

    if request.method == "GET":
        form = BilletForm(instance=billet)
        return render(request, 'doctopyt/billet.html', {"billet": billet, "form": form, "me": True})
    elif request.method == "POST":
        if request.POST.getlist("_method")[0] == "put":
            form = BilletForm(request.POST)
            if form.is_valid():
                billet.name = form.cleaned_data.get('name')
                billet.presentation = form.cleaned_data.get('presentation')
                billet.save()
            return redirect('doctopyt:me_billet', specialite_pk=specialite_pk, billet_pk=billet_pk)
        elif request.POST.getlist("_method")[0] == "delete":
            billet.delete()
            return redirect('doctopyt:me_billets', specialite_pk=specialite_pk)

@login_required(login_url='/login_user/')
@is_practicien
# Create your views here.
def createCreneau(request):
    if request.method == 'POST':
        form = CreateCreneauForm(request.POST)
        if form.is_valid():
            c = Creneau() # Definition d'un objet modele
            c.datetime_debut = form.cleaned_data['datetime_debut']
            c.datetime_fin = form.cleaned_data['datetime_fin']
            c.praticien = request.user.praticien
            c.save()
            return HttpResponseRedirect('/creneau/' + str(request.user.praticien.pk)) # Redirection vers une url
    else:
        # generation de la form initialement vide
        form = CreateCreneauForm()
    return render(request, 'doctopyt/createCreaneau.html',  {'form': form})


def visualizeCreneau(request, praticien_id):
    return render(request, 'doctopyt/visualizeCreneau.html', {'praticien_id': praticien_id, 'creneaux': Creneau.objects.filter(praticien=praticien_id).filter(datetime_debut__gte=timezone.now()).filter(rdv__isnull=True).order_by('datetime_debut')})

@login_required(login_url='/login_user/')
def reservationRdv(request, creneau_id):
    creneau = Creneau.objects.get(pk=creneau_id)
    rdv = RendezVous()
    rdv.user = request.user
    rdv.creneau = creneau
    rdv.praticien = creneau.praticien
    rdv.save()
    return HttpResponseRedirect('/rdv/' + str(request.user.id))

def visualizeRdv(request, patient_id):
    return render(request, 'doctopyt/visualizeRdv.html', {'rdvs': RendezVous.objects.filter(user=patient_id).filter(creneau__datetime_fin__gte=timezone.now()).order_by('creneau__datetime_debut')})

@login_required(login_url='/login_user/')
@require_http_methods(["GET","POST"])
def me(request):
    user = request.user

    praticien = user.praticien if hasattr(user, 'praticien') else None

    if request.method == "GET" or praticien == None:
        form = PraticienForm(instance=praticien) if praticien is not None  else None

        return render(request, 'doctopyt/me.html', {"user": user, "praticien": praticien, 'form': form})
    else:
        f = PraticienForm(request.POST)
        if f.is_valid():
            praticien = f.save(commit=False)
            praticien.user = user
            specialitiesIds = request.POST.getlist('specialities')
            praticien.specialities.set([Specialite.objects.get(pk=id) for id in specialitiesIds])
            praticien.save()
        return redirect("doctopyt:me")

@require_http_methods(["GET"])
def praticiens(request):
    search = {}
    if (request.GET.getlist('name')):
        search['name__contains'] = request.GET.getlist('name')[0]
    if (request.GET.getlist('zoneGeographique')):
        search['zoneGeographique_id'] = int(request.GET.getlist('zoneGeographique')[0])

    praticiens = []
    if (request.GET.getlist('specialities')):
        praticiens = {}
        for speciality in Specialite.objects.filter(id__in=map(int,request.GET.getlist('specialities'))):
            for praticien in speciality.praticiens.filter(**search):
                praticiens[praticien.user_id] = praticien
        praticiens = praticiens.values()
    else:
        praticiens = Praticien.objects.filter(**search)

    form = SearchPraticienForm(request.GET)
    return render(request, 'doctopyt/praticiens.html', {"praticiens": praticiens, 'form': form})

def view_praticien(request, praticien_pk):
    try:
        praticien = Praticien.objects.get(pk=praticien_pk)
    except:
        raise Http404

    return render(request, 'doctopyt/view_praticien.html', {"praticien": praticien})

def view_praticien_billets(request, praticien_pk, specialite_pk):
    try:
        praticien = Praticien.objects.get(pk=praticien_pk)
    except:
        raise Http404
    try:
        praticien_specialite = praticien.praticien_specialite.get(specialite_id=specialite_pk)
    except:
        raise Http404
    return render(request, 'doctopyt/billets.html', {
        "praticien": praticien,
        "billets": praticien_specialite.billets.all(),
        "specialite": praticien_specialite.specialite,
        "me": False
    })

def view_praticien_billet(request, praticien_pk, specialite_pk, billet_pk):
    try:
        praticien = Praticien.objects.get(pk=praticien_pk)
    except:
        raise Http404
    try:
        praticien_specialite = praticien.praticien_specialite.get(specialite_id=specialite_pk)
    except:
        raise Http404
    try:
        billet = praticien_specialite.billets.get(id=billet_pk)
    except:
        raise Http404
    return render(request, 'doctopyt/billet.html', {
        "praticien": praticien,
        "billet": billet,
        "praticien_specialite": praticien_specialite,
        "me": False
    })

def chat(request, room_name):
    return render(request, 'doctopyt/chat.html', {
        'room_name': room_name
    })
