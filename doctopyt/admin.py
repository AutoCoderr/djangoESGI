from django.contrib import admin
from .models import Praticien, Creneau, RendezVous, Billet, Specialite, ZoneGeographique


# Register your models here.
admin.site.register(Praticien)
admin.site.register(ZoneGeographique)
admin.site.register(Creneau)
admin.site.register(RendezVous)
admin.site.register(Billet)
admin.site.register(Specialite)

