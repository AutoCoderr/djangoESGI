from django.urls import path

from . import views

app_name = 'doctopyt'
urlpatterns = [
    path('creneau/create/', views.createCreneau, name='createCreneau'),
    path('creneau/<int:praticien_id>/', views.visualizeCreneau, name='visualizeCreneau'),
    path('rdv/<int:patient_id>/', views.visualizeRdv, name='visualizeRdv'),
    path('rdv/<int:creneau_id>/reservation/', views.reservationRdv, name='reservationRdv'),
    path('', views.index, name='index'),
    path('login_user/', views.login_user, name='login'),
    path('logout_user/', views.logout_user, name='logout'),
    path('register/', views.register, name='register'),
    path('me/', views.me, name="me"),
    path('me/specialites/', views.me_specialites, name='me_specialites'),
    path('me/specialites/<int:specialite_pk>/billets/', views.me_billets, name='me_billets'),
    path('me/specialites/<int:specialite_pk>/billets/<int:billet_pk>', views.me_billet, name='me_billet'),
    path('praticiens/', views.praticiens, name="praticiens"),
    path('praticiens/<int:praticien_pk>', views.view_praticien, name="view_praticien"),
    path('praticiens/<int:praticien_pk>/<int:specialite_pk>', views.view_praticien_billets, name="view_praticien_billets"),
    path('praticiens/<int:praticien_pk>/<int:specialite_pk>/<int:billet_pk>', views.view_praticien_billet, name="view_praticien_billet"),
    path('chat/<str:room_name>/', views.chat, name="chat")
]
