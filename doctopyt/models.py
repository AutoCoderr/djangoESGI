from django.db import models
from django.contrib.auth.models import User


class ZoneGeographique(models.Model):
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.name


class Specialite(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Praticien_specialite(models.Model):
    specialite = models.ForeignKey(Specialite, on_delete=models.CASCADE, related_name='praticien_specialite')
    praticien = models.ForeignKey('Praticien', on_delete=models.CASCADE, related_name='praticien_specialite')

class Billet(models.Model):
    name = models.CharField(max_length=255)
    presentation = models.TextField()
    praticien_specialite = models.ForeignKey(Praticien_specialite, on_delete=models.CASCADE, related_name='billets')

    def __str__(self):
        return self.name + ' - ' + self.presentation + ' - ' + self.specialite

class Praticien(models.Model):
    user = models.OneToOneField(
            User,
            on_delete=models.CASCADE,
            primary_key=True,
          )
    name = models.CharField(max_length=255)
    presentation = models.TextField()
    zoneGeographique = models.ForeignKey(ZoneGeographique, on_delete=models.CASCADE, default=None, null=True)
    patients = models.ManyToManyField(User, related_name='praticiens')
    specialities = models.ManyToManyField(Specialite, through="Praticien_specialite", related_name='praticiens')


class Creneau(models.Model):
    datetime_debut = models.DateTimeField()
    datetime_fin = models.DateTimeField()
    praticien = models.ForeignKey(Praticien, on_delete=models.CASCADE)


class RendezVous(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    creneau = models.ForeignKey(Creneau, on_delete=models.CASCADE, related_name='rdv')
    praticien = models.ForeignKey(Praticien, on_delete=models.CASCADE)
