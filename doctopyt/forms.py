from django import forms
from django.forms import ModelForm, ModelMultipleChoiceField, CheckboxSelectMultiple, CharField, ChoiceField, PasswordInput
from django.contrib.auth.models import User
from .models import Praticien, Specialite, ZoneGeographique, Billet
from django.core.validators import RegexValidator
from django.contrib.auth.forms import UserCreationForm


class BilletForm(ModelForm):
    class Meta:
        model = Billet
        fields = ['name','presentation']

class CreateCreneauForm(forms.Form):
    datetime_debut = forms.DateTimeField(widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control mb-3'},
            format='%Y-%m-%dT%H:%M'), label='Date de debut')
    datetime_fin = forms.DateTimeField(widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control mb-3',},
            format='%Y-%m-%dT%H:%M'), label='Date de fin')

class PraticienForm(ModelForm):
    class Meta:
        model = Praticien
        fields = ['name', 'presentation', 'zoneGeographique', 'specialities']

    specialities = ModelMultipleChoiceField(
        queryset=Specialite.objects.all(),
        widget=CheckboxSelectMultiple,
        required=False,
    )

    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    presentation = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        super(PraticienForm, self).__init__(*args, **kwargs)

        # Load choices here so db calls are not made during migrations.
        self.fields['zoneGeographique'].required = False

class SearchPraticienForm(ModelForm):
    class Meta:
        model = Praticien
        fields = ['name', 'zoneGeographique', 'specialities']

    name = CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    zoneGeographique = ChoiceField(
        choices=(('', '')),
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    specialities = ModelMultipleChoiceField(
        queryset=Specialite.objects.all(),
        widget=CheckboxSelectMultiple,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(SearchPraticienForm, self).__init__(*args, **kwargs)

        # Load choices here so db calls are not made during migrations.
        self.fields['zoneGeographique'].choices = map(
            lambda z: (z.id, z.name), ZoneGeographique.objects.all())


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'type', 'password1', 'password2']

    type = ChoiceField(
        choices=(('patient', 'Patient'), ('praticien', 'Praticien')),
        required=False
    )
